<?php

require_once 'Order.php';
require_once 'BasketPosition.php';
require_once 'Product.php';

class Basket
{

    private $aProducts;

    public function __construct()
    {
        $this->aProducts = [];
    }

    public function addProduct(Product $product, $quantity)
    {
        $oPosition = new BasketPosition($product, $quantity);
        $this->aProducts[] = $oPosition;
    }

    public function getPrice()
    {
        $fAmountOfProducts = 0;

        foreach ($this->aProducts as $product) {
            $fAmountOfProducts += $product->getPrice();
        }

        return $fAmountOfProducts;
    }

    public function describe()
    {
        $sResponse = '';

        foreach ($this->aProducts as $product) {
            $sResponse .= "<br>" . $product->getProduct() . " - "
                . $product->getPrice() . " - " . $product->getQuantity();
        }

        return $sResponse;
    }

}
