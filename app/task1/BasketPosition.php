<?php

require_once 'Basket.php';
require_once 'Order.php';
require_once 'Product.php';

class BasketPosition
{

    private Product $oProduct;
    private $iQuantity;

    public function __construct(Product $oProduct, $iQuantity)
    {
        $this->oProduct = $oProduct;
        $this->iQuantity = $iQuantity;
    }

    public function getProduct()
    {
        return $this->oProduct->getName();
    }

    public function getQuantity()
    {
        return $this->iQuantity;
    }

    public function getPrice()
    {
        return $this->oProduct->getPrice();
    }

}
