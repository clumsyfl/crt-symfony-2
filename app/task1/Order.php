<?php

require_once 'Basket.php';
require_once 'BasketPosition.php';
require_once 'Product.php';

class Order
{

    private Basket $oBasket;
    private $fPriceDelivery;

    public function __construct(Basket $oBasket, $fPriceDelivery)
    {
        $this->oBasket = $oBasket;
        $this->fPriceDelivery = $fPriceDelivery;
    }

    public function getBasket()
    {
        return $this->oBasket;
    }

    public function getPrice()
    {
        $this->oBasket->getPrice() + $this->fPriceDelivery;
    }

}
