<?php

require_once 'Basket.php';
require_once 'BasketPosition.php';
require_once 'Order.php';

class Product
{

    private $sName;
    private $fPrice;

    public function __construct($sName, $fPrice)
    {
        $this->sName = $sName;
        $this->fPrice = $fPrice;
    }

    public function getName()
    {
        return $this->sName;
    }

    public function getPrice()
    {
        return $this->fPrice;
    }

}
