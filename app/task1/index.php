<?php

require_once 'Order.php';
require_once 'Basket.php';
require_once 'BasketPosition.php';
require_once 'Product.php';

$oBasket = new Basket();

$oBasket->addProduct(new Product("Клавиатура", 3000), 1);
$oBasket->addProduct(new Product("Мышь", 2000), 2);
$oBasket->addProduct(new Product("Монитор", 5000), 2);
$oBasket->addProduct(new Product("Коврик для мыши", 1000), 1);
$oBasket->addProduct(new Product("Стул", 5000), 1);

echo "Заказ на сумму: " . $oBasket->getPrice() . "<br><br> Состав:" . $oBasket->describe();
