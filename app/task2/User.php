<?php

class User
{
    private $iId;
    private $sName;
    private $iAge;
    private $sEmail;

    public function __construct($iId, $sName, $iAge, $sEmail)
    {
        $this->iId = $iId;
        $this->sName = $sName;
        $this->iAge = $iAge;
        $this->sEmail = $sEmail;
    }

    public function getId()
    {
        return $this->iId;
    }

    public function getName()
    {
        return $this->sName;
    }

    public function getAge()
    {
        return $this->iAge;
    }

    public function getEmail()
    {
        return $this->sEmail;
    }

    public function load(int $id)
    {
        if ($id % 2 == 0) {
            throw new ErrorException("Ошибка. Id пользователя не найдено в БД (чётное)");
        }
    }

    public function save(array $data): bool
    {
        $iRand = rand(0, 1);
        $bAccept = false;
        if ($iRand == 0) {
            $bAccept = true;
        } else $bAccept = false;
        return $bAccept;
    }

}
