<?php

require_once "User.php";

class UserFormValidator
{
    public function validate(array $data)
    {
        foreach ($data as $element)
            if (empty($element->getName())) {
                throw new Error ("Имя пользователя не заполнено<br>");
            } else if ((filter_var($element->getAge(), FILTER_VALIDATE_INT)) && $element->getAge() < 18) {
                throw new Error("Возраст меньше 18 лет<br>");
            } else if (!filter_var($element->getEmail(), FILTER_VALIDATE_EMAIL)) {
                throw new Error("Введенный Email неправильного формата<br>");
            }

        return true;
    }
}