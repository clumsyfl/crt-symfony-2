<?php

require_once "User.php";
require_once "UserFormValidator.php";

session_start();
$_SESSION["iId"] = isset($_POST["id"]) ? $_POST["id"] : "";
$_SESSION["sName"] = isset($_POST["name"]) ? $_POST["name"] : "";
$_SESSION["iAge"] = isset($_POST["age"]) ? $_POST["age"] : "";
$_SESSION["sEmail"] = isset($_POST["email"]) ? $_POST["email"] : "";

try {
    $oUser = new User($_POST["id"], $_POST["name"], $_POST["age"], $_POST["email"]);
    $oUser->load($oUser->getId());
    $oUserValidator = new UserFormValidator();
    $oUserValidator->validate([$oUser]);

    if ($oUser->save([$oUser])) {
        echo "Пользователь сохранен<br/>";
        formClear();
    } else {
        throw new Error("Ошибка, пользователь не сохранен!");
    }
} catch (Error $e) {
    echo $e->getMessage();
}

function formClear()
{
    $_SESSION["iId"] = "";
    $_SESSION["sName"] = "";
    $_SESSION["iAge"] = "";
    $_SESSION["sEmail"] = "";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
<form method="post">
    <p>ID: <input type="text" name="id" value="<?php echo $_SESSION['iId']; ?>"></p>
    <p>Имя: <input type="text" name="name" value="<?php echo $_SESSION['sName']; ?>"></p>
    <p>Возраст: <input type="text" name="age" value="<?php echo $_SESSION['iAge']; ?>"></p>
    <p>Еmail: <input type="text" name="email" value="<?php echo $_SESSION['sEmail']; ?>"></p>
    <p><input type="submit"/></p>
</form>
</body>
</html>
